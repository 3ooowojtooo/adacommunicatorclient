package body Gui_Package is

  procedure Read_Server_Data(Server_Address: out SU.Unbounded_String; Server_Port: out Natural; Username: out SU.Unbounded_String) is
    package NaturalIO is new Ada.Text_IO.Integer_IO (Natural);
  begin
    Put("Enter server address: ");
    Server_Address := SU.To_Unbounded_String(Get_Line);
    loop
    begin
      Put("Enter server port: ");
      NaturalIO.Get(Server_Port);
      exit;
    exception
      when others => Skip_Line;
    end;
    end loop;

    Skip_Line;
    loop
        Put("Enter username: ");
        Username := SU.To_Unbounded_String(Get_Line);
        if (SU.To_String(Username) /= "") then
          exit;
        end if;
    end loop;
  end Read_Server_Data;

  procedure Server_Data_Accepted is
  begin
    Put_Line("Succesfully connected to the server");
  end Server_Data_Accepted;

  procedure Server_Data_Rejected is
  begin
    Put_Line("Server connection fail");
  end Server_Data_Rejected;

end Gui_Package;
