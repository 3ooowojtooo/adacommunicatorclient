package body Command_Manager is

  procedure Dispatch(Command: in String) is
    Command_Copy : String := Ada.Strings.Fixed.Trim(Command, Ada.Strings.Both);
    Command_Head : String := Get_Command_Head(Command_Copy);
    Command_Arg : String := Get_Command_Argument(Command_Copy);
  begin
    Validate_Command(Command_Head, Command_Arg);
    if Command_Head = "/List" then
      Started_Server_Controller.Command_Entered(Command_Head);
    elsif Command_Head = "/Connect" then
      Started_Server_Controller.Command_Arg_Entered(Command_Head, Command_Arg);
    elsif Command_Head = "/Accept" then
      Started_Server_Controller.Command_Entered(Command_Head);
    elsif Command_Head = "/Reject" then
      Started_Server_Controller.Command_Entered(Command_Head);
    elsif Command_Head = "/Disconnect" then
      Started_Server_Controller.Command_Entered(Command_Head);
    elsif Ada.Strings.Fixed.Index(Command_Head,"/") /= 1 then
      Started_Server_Controller.Text_Message_Entered(Command_Copy);
    end if;
    exception
      when INVALID_COMMAND => SC.Invalid_Command_Entered;
  end Dispatch;

  procedure Validate_Command(Command_Head: in String; Command_Arg: in String) is
    use SC;
    Status : SC.Status_Type := SC.Get_Status;
  begin
    if Command_Head = "/List" then
      if Status /= SC.Connected_To_Server then
        raise INVALID_COMMAND;
      end if;
    elsif Command_Head = "/Connect" then
      if Status /= SC.Connected_To_Server then
        raise INVALID_COMMAND;
      else
        if Command_Arg = "" then
          raise INVALID_COMMAND;
        end if;
      end if;
    elsif Command_Head = "/Accept" then
      if Status /= Connect_Request then
        raise INVALID_COMMAND;
      end if;
    elsif Command_Head = "/Reject" then
      if Status /= Connect_Request then
        raise INVALID_COMMAND;
      end if;
    elsif Command_Head = "/Disconnect" then
      if Status /= Connected_To_Client and Status /= Connected_To_Server then
        raise INVALID_COMMAND;
      end if;
    elsif Ada.Strings.Fixed.Index(Command_Head, "/") /= 1 then
      if Status /= Connected_To_Client then
        raise INVALID_COMMAND;
      end if;
    else
      raise INVALID_COMMAND;
    end if;
  end Validate_Command;

  function Get_Command_Head(Command: in String) return String is
    Helper: SU.Unbounded_String := SU.To_Unbounded_String(Command);
    Space_Position: Natural;
  begin
    Space_Position := SU.Index(Helper, " ");
    if Space_Position > 0 then
      SU.Delete(Helper, Space_Position, SU.Length(Helper));
    end if;
    return SU.To_String(Helper);
  end Get_Command_Head;

  function Get_Command_Argument(Command: in String) return String is
    Helper: SU.Unbounded_String := SU.To_Unbounded_String(Command);
    Space_Position: Natural;
  begin
    Space_Position := SU.Index(Helper, " ");
    if Space_Position = 0 then
      return "";
    end if;
    SU.Delete(Helper, 1, Space_Position);
    return SU.To_String(Helper);
  end Get_Command_Argument;
end Command_Manager;
