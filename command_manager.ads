with Started_Server_Controller;
with Ada.Strings.Unbounded;
with Ada.Strings.Fixed;

package Command_Manager is
  package SU renames Ada.Strings.Unbounded;
  package SC renames Started_Server_Controller;

  INVALID_COMMAND: exception;

  procedure Dispatch(Command: in String);

  private
  procedure Validate_Command(Command_Head: in String; Command_Arg: in String);
  function Get_Command_Head(Command: in String) return String;
  function Get_Command_Argument(Command: in String) return String;
end Command_Manager;
