package body Main_Controller is

  procedure Run is
    Server_Socket : Socket_Type;
    Username: SU.Unbounded_String;
  begin
    Connect_To_Server(Server_Socket, Username);
    Started_Server_Controller.Start(Server_Socket, Username);
  end;

  procedure Connect_To_Server(Server_Socket: out Socket_Type; Username_Out: out SU.Unbounded_String) is
    Server_Address : SU.Unbounded_String;
    Server_Port: Natural;
    Username: SU.Unbounded_String;
  begin
      loop
        begin
        Gui_Package.Read_Server_Data(Server_Address, Server_Port, Username);
        Server_Socket := Initialize_Server_Socket(SU.To_String(Server_Address), Server_Port, SU.To_String(Username));
        Username_Out := Username;
        Gui_Package.Server_Data_Accepted;
        exit;
        exception
          when GNAT.SOCKETS.SOCKET_ERROR => Gui_Package.Server_Data_Rejected;
        end;
      end loop;
  end Connect_To_Server;

  function Initialize_Server_Socket(Server_Address: String; Server_Port: Natural; Username: String) return Socket_Type is
    Address: Sock_Addr_Type;
    Socket: Socket_Type;
  begin
    Address.Addr := Inet_Addr(Server_Address);
    Address.Port := Port_Type(Server_Port);
    Create_Socket(Socket);
    Connect_Socket(Socket, Address);
    Hello(Socket, Username);
    return Socket;
  end Initialize_Server_Socket;

  procedure Hello(Server_Socket: Socket_Type; Username: String) is
    Hello_Message : Message;
    Channel : Stream_Access;
  begin
    Hello_Message := Create_Hello_Message(Username);
    Channel := Stream(Server_Socket);
    Message'Write(Channel, Hello_Message);
  end Hello;

end Main_Controller;
