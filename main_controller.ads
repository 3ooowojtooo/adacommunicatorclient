-- COPYRIGHT 2018 Rafał Rak, Wojciech Góralczyk

-- This file is part of adaCommunicatorServer.
--
-- adaCommunicatorServer is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- adaCommunicatorServer is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with adaCommunicatorServer.  If not, see <https://www.gnu.org/licenses/>.

with GNAT.Sockets;
use GNAT.Sockets;
with Gui_Package;
use Gui_Package;
with Data_Utils_Package;
use Data_Utils_Package;
with Started_Server_Controller;

package Main_Controller is

  procedure Run;

  private

  procedure Connect_To_Server(Server_Socket: out Socket_Type; Username_Out: out SU.Unbounded_String);
  function Initialize_Server_Socket(Server_Address: String; Server_Port: Natural; Username: String) return Socket_Type;
  procedure Hello(Server_Socket: Socket_Type; Username: String);

end Main_Controller;
