with Ada.Strings.Unbounded;
with Data_Utils_Package;
with Ada.Text_IO;
use Ada.Text_IO;

package Gui_Package is
  package SU renames Ada.Strings.Unbounded;
  package DATA renames Data_Utils_Package;

  procedure Read_Server_Data(Server_Address: out SU.Unbounded_String; Server_Port: out Natural; Username: out SU.Unbounded_String);
  procedure Server_Data_Accepted;
  procedure Server_Data_Rejected;
end Gui_Package;
