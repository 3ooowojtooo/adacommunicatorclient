package body UI_Package is

  protected body UI is
    procedure Connected_To_Server(Server_Socket: Socket_Type; Username: in String) is
    begin
      CC.Clear;
      CC.Line("Connected to: " & Image(Get_Peer_Name(Server_Socket)));
      CC.Line("Username: " & Username);
      CC.Line("/List - list logged in clients.");
      CC.Line("/Connect USERNAME - connect to username.");
      CC.Line("/Disconnect - disconnect from server.");
      CC.Start_Listening;
    end Connected_To_Server;

    procedure Disconnected_From_Server is
    begin
      CC.Stop_Listening;
    end Disconnected_From_Server;

    procedure Invalid_Command is
    begin
      CC.Line("Invalid command.");
    end Invalid_Command;

    procedure Show_Clients_List(Clients: in String_List.List) is
      Cur: String_List.Cursor;
    begin
      if String_List.Is_Empty(Clients) then
        CC.Line("There are not connected clients currently.");
      else
        CC.Line("Currently connected clients: ");
        Cur := String_List.First(Clients);
        while String_List.Has_Element(Cur) loop
          CC.Line(String_List.Element(Cur));
          String_List.Next(Cur);
        end loop;
      end if;
    end Show_Clients_List;

    procedure Trying_To_Connect(Username: in String) is
    begin
      CC.Line("Trying to connect to client " & Username);
    end Trying_To_Connect;

    procedure Client_Not_Connected is
    begin
      CC.Line("Client is not connected.");
    end Client_Not_Connected;

    procedure Connection_Impossible is
    begin
      CC.Line("Connection is impossible.");
    end Connection_Impossible;

    procedure Connect_Request(Username: in String) is
    begin
      CC.Line(Username & " is calling you. Use /Accept or /Reject");
    end Connect_Request;

    procedure Connection_Rejected is
    begin
      CC.Line("Connection rejected");
    end Connection_Rejected;

    procedure Connection_Accepted(Username: in String) is
    begin
      CC.Clear;
      CC.Line("Connected with user: " & username);
      CC.Line("/Disconnect - disconnect");
      CC.Show_Command_Line;
    end Connection_Accepted;

    procedure Message(From_Username: in String; Text_Message: in String) is
    begin
      CC.Line(From_Username & ": " & Text_Message);
    end Message;
  end UI;
end UI_Package;
