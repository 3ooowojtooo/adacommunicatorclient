package body Started_Server_Controller is

  procedure Start(Socket: Socket_Type; User: in SU.Unbounded_String) is
  begin
    Server_Socket := Socket;
    Username := User;

    UI.Connected_To_Server(Server_Socket, SU.To_String(Username));
    Set_Status(Connected_To_Server);
    Start_Server_Message_Handler;
    Start_Server_Connection_Task;

  end Start;

  procedure Stop is
  begin
    Stop_Server_Connection_Task;
    Stop_Server_Message_Handler;
    UI.Disconnected_From_Server;
    Close_Socket(Server_Socket);
  end Stop;

  procedure Command_Entered(Command: in String) is
  begin
    if Command = "/List" then
      Process_List_Command;
    elsif Command = "/Accept" then
      Process_Accept_Command;
    elsif Command = "/Reject" then
      Process_Reject_Command;
    elsif Command = "/Disconnect" then
      Process_Disconnect_Command;
    end if;
  end Command_Entered;

  procedure Command_Arg_Entered(Command: in String; Arg: in String) is
  begin
    if Command = "/Connect" then
      Process_Connect_Command(Arg);
    end if;
  end Command_Arg_Entered;

  procedure Text_Message_Entered(Text_Message: in String) is
  begin
    Connected_Client_Controller.Show_Text_Message_From_Me(Text_Message);
    Server_Connection_T.Send_Blocking(Create_Send_Message(Connected_Client_Controller.Get_Connected_Username, Text_Message));
  end Text_Message_Entered;

  procedure Invalid_Command_Entered is
  begin
    UI.Invalid_Command;
  end Invalid_Command_Entered;

  procedure Start_Server_Connection_Task is
  begin
    Server_Connection_T := new Server_Connection_Task;
    Server_Connection_T.Start(Server_Socket);
    Is_Server_Connection_Task_Running := True;
  end Start_Server_Connection_Task;

  procedure Stop_Server_Connection_Task is
    procedure Free is new Unchecked_Deallocation(Server_Connection_Task, Server_Connection_Task_PTR);
  begin
    Is_Server_Connection_Task_Running := False;
    Server_Connection_T.Stop;
    Free(Server_Connection_T);
    Server_Connection_T := null;
  end Stop_Server_Connection_Task;

  procedure Start_Server_Message_Handler is
  begin
    Server_Message_H := new Server_Message_Handler;
    Server_Message_H.Start;
    Is_Server_Message_Handler_Running := True;
  end Start_Server_Message_Handler;

  procedure Stop_Server_Message_Handler is
    procedure Free is new Unchecked_Deallocation(Server_Message_Handler, Server_Message_Handler_PTR);
  begin
    Is_Server_Connection_Task_Running := False;
    Server_Message_H.Stop;
    Free(Server_Message_H);
    Server_Message_H := null;
  end Stop_Server_Message_Handler;

  procedure Message_From_Server(Mes: in Message) is
  begin
    Server_Message_H.Message_From_Server(Mes);
  end Message_From_Server;

  function Get_Status return Status_Type is
  begin
    return Status;
  end Get_Status;

  procedure Set_Status(New_Status: Status_Type) is
  begin
    Status := New_Status;
  end Set_Status;

  procedure Process_Message_From_Server(Mes: in Message) is
    Clients : String_List.List;
    Command : String := SU.To_String(Mes.Command);
  begin
    if Command = "CLIENT_LIST" then
      if Status = Waiting_For_List_Response then
      Clients := Prepare_Clients_List(Mes.Arguments_List, SU.To_String(Username));
      UI.Show_Clients_List(Clients);
      Set_Status(Connected_To_Server);
      Console_Controller.Show_Command_Line;
      elsif Status = Waiting_For_List_Response_Connect then
        Clients := Prepare_Clients_List(Mes.Arguments_List, SU.To_String(Username));
        if String_List.Contains(Clients, SU.To_String(Commands_Processing_Argument_Helper)) then

          Set_Status(Waiting_For_Connect_Response);
          Server_Connection_T.Send_Blocking(Create_Connect_Message(SU.To_String(Username), SU.To_String(Commands_Processing_Argument_Helper)));
        else
          UI.Client_Not_Connected;
          Set_Status(Connected_To_Server);
          Console_Controller.Show_Command_Line;
        end if;
      end if;
    elsif Command = "CONNECT" then
      if Status = Connected_To_Server then
        Set_Status(Connect_Request);
        Commands_Processing_Argument_Message_Helper := Mes;
        UI.Connect_Request(Get_Argument(Mes, 2));
        Console_Controller.Show_Command_Line;
      else
        Server_Connection_T.Send_Blocking(Create_Connection_Impossible_Message(Mes));
      end if;
    elsif Command = "CONNECTION_IMPOSSIBLE" then
      if Status = Waiting_For_Connect_Response then
        UI.Connection_Impossible;
        Set_Status(Connected_To_Server);
        Console_Controller.Show_Command_Line;
      end if;
    elsif Command = "CONNECTION_REJECTED" then
      if Status = Waiting_For_Connect_Response then
        UI.Connection_Rejected;
        Set_Status(Connected_To_Server);
        Console_Controller.Show_Command_Line;
      end if;
    elsif Command = "CONNECTION_ACCEPTED" then
      if Status = Waiting_For_Connect_Response then
        Connected_Client_Controller.Init(SU.To_String(Username), Get_Argument(Mes, 2));
        Server_Connection_T.Send_Blocking(Create_Ready_Message(Mes));
        Set_Status(Connected_To_Client);
        Connected_Client_Controller.Display_Beggining;
      end if;
    elsif Command = "READY" then
      if Status = Waiting_For_Ready then
        Set_Status(Connected_To_Client);
        Connected_Client_Controller.Display_Beggining;
      end if;
    elsif Command = "SEND" then
      if Status = Connected_To_Client then
        Connected_Client_Controller.Show_Text_Message_From_Connected(Get_Argument(Mes, 2));
      end if;
    elsif Command = "DISCONNECT" then
      if Status = Connected_To_Client then
        Console_Controller.Hide_Command_Line;
        UI.Connected_To_Server(Server_Socket, SU.To_String(Username));
        Set_Status(Connected_To_Server);
        Console_Controller.Show_Command_Line;
      end if;
    end if;
  end Process_Message_From_Server;

 procedure Process_List_Command is
 begin
   Console_Controller.Hide_Command_Line;
   Set_Status(Waiting_For_List_Response);
   Server_Connection_T.Send_Blocking(Create_List_Clients_Message);
 end Process_List_Command;

 procedure Process_Connect_Command(Arg: in String) is
 begin
   Console_Controller.Hide_Command_Line;
   Set_Status(Waiting_For_List_Response_Connect);
   UI.Trying_To_Connect(Arg);
   Commands_Processing_Argument_Helper := SU.To_Unbounded_String(Arg);
   Server_Connection_T.Send_Blocking(Create_List_Clients_Message);
 end Process_Connect_Command;

 procedure Process_Accept_Command is
 begin
   Console_Controller.Hide_Command_Line;
   Server_Connection_T.Send_Blocking(Create_Connection_Accepted_Message(Commands_Processing_Argument_Message_Helper));
   Connected_Client_Controller.Init(Get_Argument(Commands_Processing_Argument_Message_Helper, 1), Get_Argument(Commands_Processing_Argument_Message_Helper,2));
   Set_Status(Waiting_For_Ready);
 end Process_Accept_Command;

 procedure Process_Reject_Command is
 begin
   Server_Connection_T.Send_Blocking(Create_Connection_Rejected_Message(Commands_Processing_Argument_Message_Helper));
   UI.Connection_Rejected;
   Set_Status(Connected_To_Server);
   Console_Controller.Show_Command_Line;
 end Process_Reject_Command;

procedure Process_Disconnect_Command is
begin
  if Status = Connected_To_Client then
    Console_Controller.Hide_Command_Line;
    Server_Connection_T.Send_Blocking(Create_Disconnect_Message(Connected_Client_Controller.Get_Connected_Username));
    UI.Connected_To_Server(Server_Socket, SU.To_String(Username));
    Set_Status(Connected_To_Server);
    Console_Controller.Show_Command_Line;
  elsif Status = Connected_To_Server then
    Stop;
  end if;
end Process_Disconnect_Command;

  task body Server_Message_Handler is
    Current_Message : Message;
  begin
    accept Start;
    loop
      select
        accept Message_From_Server(Msg: in Message) do
          Current_Message := Msg;
        end Message_From_Server;
        Started_Server_Controller.Process_Message_From_Server(Current_Message);
       or
        accept Stop;
        exit;
      end select;
    end loop;
  end Server_Message_Handler;

  task body Server_Connection_Task is
    Request_Non_Blocking: Request_Type(Name => Non_Blocking_IO);
    Request_Blocking: Request_Type(Name => Non_Blocking_IO);
    Channel : Stream_Access;
    Read_Message: Message;
    Server_Socekt: Socket_Type;
  begin
    Request_Non_Blocking.Enabled := True;
    Request_Blocking.Enabled := False;

    accept Start(Server: Socket_Type) do
      Server_Socekt := Server;
    end Start;
    Control_Socket(Server_Socket, Request_Non_Blocking);
    Channel := Stream(Server_Socket);
    loop
      select
       accept Stop;
       exit;
      or
       accept Send_Blocking(Mes: in Message) do
        Control_Socket(Server_Socket, Request_Blocking);
        Message'Write(Channel, Mes);
        Control_Socket(Server_Socket, Request_Non_Blocking);
       end Send_Blocking;
      or
        delay 0.2;
        begin
        Control_Socket(Server_Socket, Request_Non_Blocking);
        Message'Read(Channel, Read_Message);
        Started_Server_Controller.Message_From_Server(Read_Message);
        exception
          when SOCKET_ERROR => null;
          when others => null;
        end;
      end select;
    end loop;
  end Server_Connection_Task;
end Started_Server_Controller;
