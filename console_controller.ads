with Ada.Strings.Unbounded;
with Ada.Strings;
use Ada.Strings;
with Ada.Strings.Fixed;
use Ada.Strings.Fixed;
with Ada.Text_IO;
with Unchecked_Deallocation;

package Console_Controller is

  procedure Clear;
  procedure Line(Str: in String);
  procedure Hide_Command_Line;
  procedure Show_Command_Line;
  procedure Render_Command_Line;
  procedure Start_Listening;
  procedure Stop_Listening;


  private
  procedure Command_Character_Pressed(Char: in Character);
  procedure Clear_Command_Line;

  protected Screen is
    procedure Put_String_XY(X,Y: in Positive; S: in String);
    procedure Put(Str: in String);
    procedure Put(Char: in Character);
    procedure Get_Immediate(Char: out Character; Available: out Boolean);
    procedure Delete_Character;
    procedure New_Line;

    private
    function Esc_XY(X,Y : in Positive) return String;

  end Screen;

  task type Key_Press_Listener is
    entry Start;
  end Key_Press_Listener;

  Current_Y : Positive := 1 with Atomic;
  Command_Buffer : Ada.Strings.Unbounded.Unbounded_String;
  Is_Listener_Running : Boolean := False with Atomic;
  type Listener_PTR is access Key_Press_Listener;
  Listener : Listener_PTR := null;
  Is_Command_Line_Allowed_To_Show : Boolean := True with Atomic;

end Console_Controller;
