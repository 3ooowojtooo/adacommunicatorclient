with GNAT.Sockets;
use GNAT.Sockets;
with Ada.Strings.Unbounded;
with UI_Package;
use UI_Package;
with Data_Utils_Package;
use Data_Utils_Package;
with Console_Controller;
with Connected_Client_Controller;
with Ada.IO_EXCEPTIONS;
with Unchecked_Deallocation;

package Started_Server_Controller is
  package SU renames Ada.Strings.Unbounded;
  type Status_Type is (Connected_To_Server, Waiting_For_List_Response,
  Waiting_For_List_Response_Connect, Waiting_For_Connect_Response,
  Connect_Request, Waiting_For_Ready, Connected_To_Client);

  procedure Start(Socket: Socket_Type; User: in SU.Unbounded_String);
  procedure Stop;

  procedure Command_Entered(Command: in String);
  procedure Command_Arg_Entered(Command: in String; Arg: in String);
  procedure Text_Message_Entered(Text_Message: in String);
  procedure Invalid_Command_Entered;

  function Get_Status return Status_Type;

  private

  procedure Start_Server_Connection_Task;
  procedure Stop_Server_Connection_Task;
  procedure Start_Server_Message_Handler;
  procedure Stop_Server_Message_Handler;
  procedure Message_From_Server(Mes: in Message);
  procedure Set_Status(New_Status: in Status_Type);

  procedure Process_Message_From_Server(Mes: in Message);
  procedure Process_List_Command;
  procedure Process_Connect_Command(Arg: in String);
  procedure Process_Accept_Command;
  procedure Process_Reject_Command;
  procedure Process_Disconnect_Command;

  task type Server_Connection_Task is
    entry Start(Server: Socket_Type);
    entry Send_Blocking(Mes: in Message);
    entry Stop;
  end Server_Connection_Task;

  task type Server_Message_Handler is
    entry Start;
    entry Message_From_Server(Msg: in Message);
    entry Stop;
  end Server_Message_Handler;

  Server_Socket: Socket_Type;
  Username: SU.Unbounded_String;

  type Server_Connection_Task_PTR is access Server_Connection_Task;
  Server_Connection_T : Server_Connection_Task_PTR := null;
  Is_Server_Connection_Task_Running : Boolean := False with Atomic;

  type Server_Message_Handler_PTR is access Server_Message_Handler;
  Server_Message_H : Server_Message_Handler_PTR := null;
  Is_Server_Message_Handler_Running : Boolean := False with Atomic;

  Status : Status_Type with Atomic;
  Commands_Processing_Argument_Helper : SU.Unbounded_String;
  Commands_Processing_Argument_Message_Helper : Message;
end Started_Server_Controller;
