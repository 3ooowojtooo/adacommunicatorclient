with Ada.Strings;
use Ada.Strings;
with Ada.Strings.Fixed;
use Ada.Strings.Fixed;
with Ada.Text_IO;
with Command_Manager;
with Unchecked_Deallocation;

package body Console_Controller is

  package IO renames Ada.Text_IO;
  package SU renames Ada.Strings.Unbounded;

  procedure Clear is
  begin
    Screen.Put(ASCII.ESC & "[2J");
    Current_Y := 1;
    Command_Buffer := SU.To_Unbounded_String("");
    Screen.Put_String_XY(1,1,"");
  end Clear;

  procedure Line(Str: in String) is
  begin
    Clear_Command_Line;
    Screen.Put_String_XY(1,Current_Y,Str);
    Screen.New_Line;
    Current_Y := Current_Y + 1;
    Render_Command_Line;
    Screen.New_Line;
    Screen.Put_String_XY(SU.Length(Command_Buffer)+1, Current_Y+1, "");
  end Line;

  procedure Render_Command_Line is
  begin
    if Is_Command_Line_Allowed_To_Show then
      Screen.Put_String_XY(1,Current_Y+1,SU.To_String(Command_Buffer));
    end if;
  end Render_Command_Line;

  procedure Clear_Command_Line is
  begin
    if SU.Length(Command_Buffer) > 0 then
      Screen.Put_String_XY(SU.Length(Command_Buffer) + 1,Current_Y+1,"");
      for i in 1..SU.Length(Command_Buffer) loop
        Screen.Delete_Character;
      end loop;
    end if;
  end Clear_Command_Line;

  procedure Show_Command_Line is
  begin
    Is_Command_Line_Allowed_To_Show := True;
    Clear_Command_Line;
    Render_Command_Line;
  end Show_Command_Line;

  procedure Hide_Command_Line is
  begin
    Is_Command_Line_Allowed_To_Show := False;
    Clear_Command_Line;
  end Hide_Command_Line;

  procedure Start_Listening is
  begin
    if not Is_Listener_Running then
      Is_Listener_Running := True;
    Listener := new Key_Press_Listener;
    Listener.Start;
   end if;
  end Start_Listening;

  procedure Stop_Listening is
    procedure Free is new Unchecked_Deallocation(Key_Press_Listener, Listener_PTR);
  begin
    Is_Listener_Running := False;
    Free(Listener);
    Listener := null;
  end Stop_Listening;

  procedure Command_Character_Pressed(Char: in Character) is
    Char_Code: Natural := Character'Pos(Char);
  begin
    Screen.Delete_Character;
    if Is_Command_Line_Allowed_To_Show then
    case Char_Code is
      when 10 => -- RETURN CODE
       Command_Manager.Dispatch(SU.To_String(Command_Buffer));
       Clear_Command_Line;
       Command_Buffer := SU.To_Unbounded_String("");
      when others =>
        SU.Append(Command_Buffer, Char);
    end case;
    Render_Command_Line;
  end if;
  end Command_Character_Pressed;

  protected body Screen is
    procedure Put(Str: in String) is begin
      IO.Put(Str);
    end Put;

    procedure Put(Char: in Character) is
    begin
      IO.Put(Char);
    end Put;

    procedure Put_String_XY(X,Y: in Positive; S: in String) is
    begin
      Put( Esc_XY(X,Y) & S);
      Put( ASCII.ESC & "[0m");
    end Put_String_XY;

    function Esc_XY(X,Y : Positive) return String is
      ( (ASCII.ESC & "[" & Trim(Y'Img,Both) & ";" & Trim(X'Img,Both) & "H") );

    procedure Get_Immediate(Char: out Character; Available: out Boolean) is
    begin
      IO.Get_Immediate(Char, Available);
    end Get_Immediate;

    procedure Delete_Character is
    begin
      Put(ASCII.ESC & "[D" & " " & ASCII.ESC & "[D");
    end;

    procedure New_Line is
    begin
      IO.New_Line;
    end New_Line;
  end Screen;

  task body Key_Press_Listener is
    Char: Character;
    Available: Boolean;
  begin
    accept Start;
    loop
      if Is_Listener_Running = False then
        exit;
      end if;
       Screen.Get_Immediate(Char, Available);
       if Available then
         Console_Controller.Command_Character_Pressed(Char);
       end if;
    end loop;
  end Key_Press_Listener;

end Console_Controller;
