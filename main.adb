-- COPYRIGHT 2018 Rafał Rak, Wojciech Góralczyk

-- This file is part of adaCommunicatorServer.
--
-- adaCommunicatorServer is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- adaCommunicatorServer is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with adaCommunicatorServer.  If not, see <https://www.gnu.org/licenses/>.

with Main_Controller;
with Console_Controller;
with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Strings.Fixed;

procedure main is
begin
  Main_Controller.Run;
end main;
