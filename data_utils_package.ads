-- COPYRIGHT 2018 Rafał Rak, Wojciech Góralczyk

-- This file is part of adaCommunicatorServer.
--
-- adaCommunicatorServer is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- adaCommunicatorServer is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with adaCommunicatorServer.  If not, see <https://www.gnu.org/licenses/>.

with Ada.Strings.Unbounded;
with Ada.Streams;
with Ada.Containers.Indefinite_Doubly_Linked_Lists;
use Ada.Containers;


package Data_Utils_Package is
  package String_List is new Indefinite_Doubly_Linked_Lists(String);
  use String_List;

  type Message is record
    Command: Ada.Strings.Unbounded.Unbounded_String;
    No_Of_Arguments: Natural := 0;
    Arguments_List : String_List.List;
  end record;

  procedure Read(Input_Stream: access Ada.Streams.Root_Stream_Type'Class; Data : out Message);
  procedure Write(OutputStream: access Ada.Streams.Root_Stream_Type'Class; Data : in Message);
  for Message'Read use Read;
  for Message'Write use Write;

  function Get_Argument(Mes: in Message; Which_Arg: Positive := 1) return String;

  function Create_Hello_Message(Username: in String) return Message;
  function Create_List_Clients_Message return Message;
  function Create_Connect_Message(From_Username: in String; To_Username: in String) return Message;
  function Create_Connection_Impossible_Message(Connect_Message: in Message) return Message;
  function Create_Connection_Rejected_Message(Connect_Message: in Message) return Message;
  function Create_Connection_Accepted_Message(Connect_Message: in Message) return Message;
  function Create_Ready_Message(Accept_Message: in Message) return Message;
  function Create_Send_Message(Username: in String; Message_To_Send: in String) return Message;
  function Create_Disconnect_Message(Username: in String) return Message;

  function Prepare_Clients_List(Clients: in String_List.List; Username: in String) return String_List.List;

  private
  procedure Add_Argument(Current_Message : in out Message; Argument : String);
  function Copy_List(Source: in String_List.List) return String_List.List;

end Data_Utils_Package;
