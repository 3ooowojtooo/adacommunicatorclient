package body Connected_Client_Controller is

  procedure Init(My_Username: in String; Connected_Username: in String) is
  begin
    Username := SU.To_Unbounded_String(My_Username);
    Connected_Client_Username := SU.To_Unbounded_String(Connected_Username);
  end Init;

  procedure Display_Beggining is
  begin
    UI.Connection_Accepted(SU.To_String(Connected_Client_Username));
  end Display_Beggining;

  procedure Show_Text_Message_From_Me(Text_Message: in String) is
  begin
    UI.Message(SU.To_String(Username), Text_Message);
  end Show_Text_Message_From_Me;

  procedure Show_Text_Message_From_Connected(Text_Message: in String) is
  begin
    UI.Message(SU.To_String(Connected_Client_Username), Text_Message);
  end Show_Text_Message_From_Connected;

  function Get_Connected_Username return String is
  begin
    return SU.To_String(Connected_Client_Username);
  end Get_Connected_Username;

end Connected_Client_Controller;
