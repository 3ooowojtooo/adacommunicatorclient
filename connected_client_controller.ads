with Ada.Strings.Unbounded;
with UI_Package;
use UI_Package;

package Connected_Client_Controller is
  package SU renames Ada.Strings.Unbounded;

  procedure Init(My_Username: in String; Connected_Username: in String);
  procedure Display_Beggining;

  procedure Show_Text_Message_From_Me(Text_Message: in String);
  procedure Show_Text_Message_From_Connected(Text_Message: in String);

  function Get_Connected_Username return String;


  private
  Username: SU.Unbounded_String;
  Connected_Client_Username: SU.Unbounded_String;
end Connected_Client_Controller;
