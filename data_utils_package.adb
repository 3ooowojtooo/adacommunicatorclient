-- COPYRIGHT 2018 Rafał Rak, Wojciech Góralczyk

-- This file is part of adaCommunicatorServer.
--
-- adaCommunicatorServer is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- adaCommunicatorServer is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with adaCommunicatorServer.  If not, see <https://www.gnu.org/licenses/>.

package body Data_Utils_Package is

  package SU renames Ada.Strings.Unbounded;

  procedure Add_Argument(Current_Message : in out Message; Argument : String) is
  begin
    Current_Message.No_Of_Arguments := Current_Message.No_Of_Arguments + 1;
    String_List.Insert(Current_Message.Arguments_List, No_Element, Argument);
  end Add_Argument;

  procedure Read(Input_Stream: access Ada.Streams.Root_Stream_Type'Class; Data : out Message) is
    CurrentArgument : SU.Unbounded_String;
  begin
    String_List.Clear(Data.Arguments_List);
    Data.Command := SU.To_Unbounded_String(String'Input(Input_Stream));
    Natural'Read(Input_Stream, Data.No_Of_Arguments);
    for Iterator in 1..Data.No_Of_Arguments loop
      CurrentArgument := SU.To_Unbounded_String(String'Input(Input_Stream));
      String_List.Insert(Data.Arguments_List, No_Element, SU.To_String(CurrentArgument), 1);
    end loop;
  end Read;

  procedure Write(OutputStream: access Ada.Streams.Root_Stream_Type'Class; Data : in Message) is
    Current_Cursor : String_List.Cursor;
  begin
    String'Output(OutputStream, SU.To_String(Data.Command));
    Natural'Write(OutputStream, Data.No_Of_Arguments);
    Current_Cursor := String_List.First(Data.Arguments_List);
    while Current_Cursor /= No_Element loop
      String'Output(OutputStream, Element(Current_Cursor));
      String_List.Next(Current_Cursor);
    end loop;
  end Write;

  function Create_Hello_Message(Username: in String) return Message is
    Created_Message: Message;
  begin
    Created_Message.Command := SU.To_Unbounded_String("HELLO");
    Add_Argument(Created_Message, Username);
    return Created_Message;
  end Create_Hello_Message;

  function Create_List_Clients_Message return Message is
    Created_Message: Message;
  begin
    Created_Message.Command := SU.To_Unbounded_String("CLIENT_LIST");
    return Created_Message;
  end Create_List_Clients_Message;

  function Create_Connect_Message(From_Username: in String; To_Username: in String) return Message is
    Created_Message: Message;
  begin
    Created_Message.Command := SU.To_Unbounded_String("CONNECT");
    Add_Argument(Created_Message, To_Username);
    Add_Argument(Created_Message, From_Username);
    return Created_Message;
  end Create_Connect_Message;

  function Create_Connection_Impossible_Message(Connect_Message: in Message) return Message is
    Created_Message: Message;
    Cur: String_List.Cursor;
    To_Username: SU.Unbounded_String;
    From_Username: SU.Unbounded_String;
  begin
    Cur := String_List.First(Connect_Message.Arguments_List);
    From_Username := SU.To_Unbounded_String(String_List.Element(Cur));
    String_List.Next(Cur);
    To_Username := SU.To_Unbounded_String(String_List.Element(Cur));
    Created_Message.Command := SU.To_Unbounded_String("CONNECTION_IMPOSSIBLE");
    Add_Argument(Created_Message,SU.To_String(To_Username));
    Add_Argument(Created_Message,SU.To_String(From_Username));
    return Created_Message;
  end Create_Connection_Impossible_Message;

  function Create_Connection_Rejected_Message(Connect_Message: in Message) return Message is
    Created_Message: Message;
  begin
    Created_Message.Command := SU.To_Unbounded_String("CONNECTION_REJECTED");
    Add_Argument(Created_Message, Get_Argument(Connect_Message,2));
    Add_Argument(Created_Message, Get_Argument(Connect_Message,1));
    return Created_Message;
  end Create_Connection_Rejected_Message;

  function Create_Connection_Accepted_Message(Connect_Message: in Message) return Message is
    Created_Message: Message;
  begin
    Created_Message.Command := SU.To_Unbounded_String("CONNECTION_ACCEPTED");
    Add_Argument(Created_Message, Get_Argument(Connect_Message,2));
    Add_Argument(Created_Message, Get_Argument(Connect_Message,1));
    return Created_Message;
  end Create_Connection_Accepted_Message;

  function Create_Ready_Message(Accept_Message: in Message) return Message is
    Created_Message: Message;
  begin
    Created_Message.Command := SU.To_Unbounded_String("READY");
    Add_Argument(Created_Message, Get_Argument(Accept_Message,2));
    Add_Argument(Created_Message, Get_Argument(Accept_Message,1));
    return Created_Message;
  end Create_Ready_Message;

  function Create_Send_Message(Username: in String; Message_To_Send: in String) return Message is
    Created_Message: Message;
  begin
    Created_Message.Command := SU.To_Unbounded_String("SEND");
    Add_Argument(Created_Message, Username);
    Add_Argument(Created_Message, Message_To_Send);
    return Created_Message;
  end Create_Send_Message;

  function Create_Disconnect_Message(Username: in String) return Message is
    Created_Message : Message;
  begin
    Created_Message.Command := SU.To_Unbounded_String("DISCONNECT");
    Add_Argument(Created_Message, Username);
    return Created_Message;
  end Create_Disconnect_Message;

  function Prepare_Clients_List(Clients: in String_List.List; Username: in String) return String_List.List is
    Cur : String_List.Cursor;
    Helper_Clients : String_List.List := Copy_List(Clients);
  begin
    if String_List.Contains(Helper_Clients, Username) then
      Cur := String_List.Find(Helper_Clients, Username);
      String_List.Delete(Helper_Clients, Cur);
    end if;
    return Helper_Clients;
  end Prepare_Clients_List;

  function Copy_List(Source: in String_List.List) return String_List.List is
    Output: String_List.List;
    Cur: String_List.Cursor;
  begin
    Cur := String_List.First(Source);
    while String_List.Has_Element(Cur) loop
      String_List.Append(Output, Element(Cur));
      String_List.Next(Cur);
    end loop;
    return Output;
  end Copy_List;

  function Get_Argument(Mes: in Message; Which_Arg: Positive := 1) return String is
    Cur: String_List.Cursor;
    Counter: Integer := 1;
  begin
    Cur := String_List.First(Mes.Arguments_List);
    while Counter < Which_Arg loop
      String_List.Next(Cur);
      Counter := Counter + 1;
    end loop;
    if String_List.Has_Element(Cur) then
      return String_List.Element(Cur);
    else
      return "";
    end if;
  end Get_Argument;
end Data_Utils_Package;
