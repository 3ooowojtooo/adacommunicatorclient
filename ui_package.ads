with Console_Controller;
with GNAT.Sockets;
use GNAT.Sockets;
with Data_Utils_Package;
use Data_Utils_Package;

package UI_Package is
  protected UI is
    procedure Connected_To_Server(Server_Socket: Socket_Type; Username: in String);
    procedure Disconnected_From_Server;
    procedure Invalid_Command;
    procedure Show_Clients_List(Clients: in String_List.List);
    procedure Trying_To_Connect(Username: in String);
    procedure Client_Not_Connected;
    procedure Connection_Impossible;
    procedure Connect_Request(Username: in String);
    procedure Connection_Rejected;
    procedure Connection_Accepted(Username: in String);
    procedure Message(From_Username: in String; Text_Message: in String);
  end UI;
  private
  package CC renames Console_Controller;
end UI_Package;
